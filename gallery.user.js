// ==UserScript==
// @name        Oppaitime Torrent Gallery
// @author      Akko
// @namespace   akko@oppaiti.me
// @description Adds exhentai like gallery view to pages with torrent groups
// @include     http*://oppaiti.me/torrents.php*
// @include     http*://oppaiti.me/artist.php*
// @exclude     http*://oppaiti.me/torrents.php?id*
// @version     0.6
// @grant       none
// @downloadURL https://bitbucket.org/asutekku/oppaitime-gallery/raw/master/gallery.user.js
// @icon        http://oppaiti.me/favicon.ico
// ==/UserScript==
let releases = $('[data-cover]');
let releaseCovers = "";
let tags = $("ul.stats").children();
let hiddenTags = [];

let torrentWidth,
    torrentsPerRow;

function init() {
    initGallery();
    loadPreferences();
    setStyle();
    releaseCovers = $(".releaseDetails");
};

loadPreferences = function() {
    let galleryState = localStorage.getItem("galleryState");
    galleryState == "hidden" ? toggleMode() : "";
};

toggleMode = function() {
    if ($("#collageBody").css('display') == 'none') {
        $("#collageBody").toggle();
        $(".torrent_table").hide();
        localStorage.setItem("galleryState", "visible");
    } else {
        $("#collageBody").toggle();
        $(".torrent_table").show();
        localStorage.setItem("galleryState", "hidden");
    }
};

addButton = function(buttonText, buttonAction, buttonID) {
    return `<a class="brackets" id=${buttonID} style="float:right; margin-left:5px" onclick=${buttonAction}>${buttonText}</a>`;
};

initGallery = function() {
    let box_pad = $(`.box_pad:eq(${0})`),
        artist_information = $("#artist_information"),
        linkBox = $(`.linkbox:eq(${0})`),
        container = document.createElement("div"),
        info = document.createElement("div"),
        body = document.createElement("div");
    $(container).attr("id", "gallery_view").append(info, body);
    $(info).addClass("head").attr("id", "info").append("<strong>Gallery</strong>" + addButton("Toggle", "toggleMode()", "toggleButton"));
    $(body).attr("id", "collageBody");
    if ($(artist_information).length) {
        $(container).insertAfter(artist_information);
    } else if ($(box_pad).length) {
        $(container).insertAfter(box_pad);
    } else if ($(linkBox).length) {
        $(container).insertAfter(linkBox);
    }
    $(".torrent_table").hide();
    let containerWidth = $("#collageBody").parent("div").width();
    torrentsPerRow = Math.floor(containerWidth / 160);
    torrentWidth = Math.floor(containerWidth / torrentsPerRow);
    initGroup();
};

const artistName = function(index) {
    try {
        return $(".torrent_artists").eq(index).children().first();
    } catch (e) {
        return "Various";
    }
};

const title = function(nth) {
    let release = $(releases).eq(nth).html(),
        releaseURL = $(releases).eq(nth).attr("href"),
        artist = artistName(nth).length == 0 ? "" : artistName(nth);
    try {
        let artistURL = $(artist).attr('href');
        return `<a href=${artistURL}>${artist.html()}</a> - <a href=${releaseURL}>${release}</a>`;
    } catch (e) {
        return `<a href=${releaseURL}>${release}</a>`;
    }
};

initGroup = function() {
    for (let i = 0; i < releases.length; i++) {
        let nth = i,
            groupWrapper = document.createElement("div"),
            releaseTitle = document.createElement("div"),
            releaseMedia = document.createElement("div"),
            imageLink = document.createElement("a"),
            image = document.createElement("img"),
            releaseDetails = document.createElement("div");
        $(groupWrapper).addClass("groupWrapper").append(releaseTitle, releaseMedia, releaseDetails);;
        $(releaseMedia).addClass("releaseMedia").append(imageLink);
        $(releaseTitle).addClass("releaseTitle head").html(title(nth));
        if ($(releases).eq(nth).attr('data-cover') !== ""){
            $(image).addClass("coverImage").attr("src", $(releases).eq(nth).attr('data-cover'));
        }else{
            $(image).addClass("coverImage").attr("src", "/static/common/noartwork/nocover.png");
        }
        
        $(releaseDetails).addClass("box releaseDetails").html($(".tags").eq(nth).html());
        $(imageLink).attr("href", $(releases).eq(nth).attr('href')).append(image)
        $("#collageBody").append(groupWrapper);
    }
};

/////////////////////////////////////////
//GALLERY TOGGLING
/////////////////////////////////////////

xButton = function(buttonAction, buttonID) {
    return `<a id=${buttonID} style="float:left;" onclick=${buttonAction}>[x]</a>`;
};

$.each(tags, function(index, value) {
    let tagValue = $(value).children().text();
    tagValue = tagValue.split('.').join("_");
    $(value).children().before(xButton("toggleVisibility('" + tagValue + "')", "remove" + tagValue));
});

checkState = function(toCheck) {
    if ($('.' + toCheck + 'Group > a').hasClass("hiddenType")) {
        $.each(hiddenTags, function(index, value) {
            if (hiddenTags[index] === toCheck) hiddenTags.splice(index, 1);
        });
        $('.' + toCheck + 'Group > a').removeClass("hiddenType");
        $("#remove" + toCheck).parent().removeClass(toCheck + "Group hiddenType");
    } else {
        $("#remove" + toCheck).parent().addClass(toCheck + "Group hiddenType");
        $('.' + toCheck + 'Group > a').addClass("hiddenType");
        hiddenTags.push(toCheck);
    }
}

toggleVisibility = function(toRemove) {
    checkState(toRemove);
    let fixedString = toRemove.split('_').join(".");
    $.each(releaseCovers, function(index, value) {
        if ($(value).text().indexOf(fixedString) > -1) {
            if (hiddenTags.includes(toRemove) === true) {
                releaseCovers.eq(index).parent().hide();
            } else {
                let readyToShow = true;
                let CheckString = $(value).html();
                $.each(hiddenTags, function(jindex, value) {
                    if (CheckString.indexOf(hiddenTags[jindex]) !== -1) {
                        readyToShow = false;
                    };
                });
                if (readyToShow) {
                    releaseCovers.eq(index).parent().show();
                }
            }
        }
    })
}
/////////////////////////////////////////////

setStyle = function() {$("<style>").prop("type", "text/css").html(".groupWrapper{height:auto;width:" + torrentWidth + "px;box-sizing:border-box;float:left;overflow:hidden;padding:3px;margin:0}.releaseMedia{height:auto;width:auto;overflow:hidden}.coverImage{display:block;position:relative;width:100%;height:auto}.releaseDetails{font-size:7pt;overflow:hidden;line-height:1.5em;height:2.9em;margin-bottom:0;padding:3px}#collageBody{position:relative;display:grid;grid-template-columns:repeat(" + torrentsPerRow + ",minmax(" + torrentWidth + "px,1fr));grid-auto-columns:max-content;grid-template-rows:repeat(2);float:left}.releaseTitle{text-align:center;fontSize:8pt;display:block;padding:10px;overflow:hidden;line-height:12px;white-space:nowrap;text-overflow:ellipsis}.hiddenType{color:#525252 !important;text-decoration:line-through}").appendTo("head");};

init();